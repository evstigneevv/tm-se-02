package ru.evstigneev.tm;

import ru.evstigneev.tm.service.ProjectTaskManager;

import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        System.out.println("Welcome to Task Manager!");
        ProjectTaskManager taskManager = new ProjectTaskManager();
        System.out.print("input command or quit to exit...");
        Scanner scanner = new Scanner(System.in);
        String inputString;
        do {
            inputString = scanner.nextLine();
            switch (inputString.toLowerCase()) {
                case "help":
                    System.out.println("List of commands: ");
                    System.out.println("help - show available commands");
                    System.out.println("cp - create new project");
                    System.out.println("ct - create new task for current project");
                    System.out.println("st - show current project's tasks");
                    System.out.println("spl - show project list");
                    System.out.println("sp - show current project");
                    System.out.println("swpr - switch project");
                    System.out.println("dp - delete project by name");
                    System.out.println("up - update current project");
                    System.out.println("utn - update current project's task's name");
                    System.out.println("dt - delete current project's task");
                    break;
                case "cp":
                    System.out.print("input new project name: ");
                    inputString = scanner.nextLine();
                    System.out.println("project \"" + taskManager.createProject(inputString).getName() + " \" created ");
                    break;
                case "ct":
                    System.out.println("input new task name into current project: ");
                    inputString = scanner.nextLine();
                    System.out.println("task \"" + taskManager.createTask(inputString).getName() + " \" created ");
                    break;
                case "st":
                    taskManager.showCurrentProjectTasks();
                    break;
                case "spl":
                    taskManager.showProjectList();
                    break;
                case "sp":
                    taskManager.showCurrentProject();
                    break;
                case "swpr":
                    taskManager.showProjectList();
                    System.out.println("input project's name: ");
                    inputString = scanner.nextLine();
                    taskManager.switchProject(inputString);
                    break;
                case "dp":
                    taskManager.showProjectList();
                    inputString = scanner.nextLine();
                    taskManager.deleteProject(inputString);
                    break;
                case "up":
                    inputString = scanner.nextLine();
                    taskManager.updateProject(inputString);
                    break;
                case "utn":
                    taskManager.updateTaskName(scanner.nextLine());
                    break;
                case "dt":
                    inputString = scanner.nextLine();
                    taskManager.deleteTask(inputString);
                    break;
            }
        }
        while (!inputString.equals("quit"));
    }

}

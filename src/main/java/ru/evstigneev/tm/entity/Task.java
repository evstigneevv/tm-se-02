package ru.evstigneev.tm.entity;

public class Task {

    private String name;
    private String taskId;

    public Task(String name, String taskId) {
        this.name = name;
        this.taskId = taskId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
}

